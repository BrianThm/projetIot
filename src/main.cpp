#include <Arduino.h>
//lib for Wifi connexion
#ifdef ESP32
  #include <WiFi.h>
#else
  #include <ESP8266WiFi.h>
#endif
#include <WiFiClientSecure.h>
#include <UniversalTelegramBot.h>   // Universal Telegram Bot Library written by Brian Lough: https://github.com/witnessmenow/Universal-Arduino-Telegram-Bot
#include <ArduinoJson.h>
#include <iostream>
#include <string>
#include <sstream>

//Library for the screen
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_BusIO_Register.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

//library for the time
#include <time.h>

//lib for NTP 
#include <ezTime.h>

//Library qui permet de stocker dans la RAM (pour les images)
#include <pgmspace.h>

//Permet d'importer les différentes images au format Bitmap que l'on a mis dans le fichier /include/image.h 
#include <image.h>

// Replace with your network credentials
const char* ssid = "AndroidLAP";
const char* password = "rcfd5648";

// Initialize Telegram BOT
#define BOTtoken "1579405559:AAE34ZiWT7Ac0PTjwrQhxWW_CSrVEKVDdKo"  // your Bot Token (Get from Botfather)


// Use @myidbot to find out the chat ID of an individual or a group
// Also note that you need to click "start" on a bot before it can
// message you
#define CHAT_ID "-411319356"

//wifi Initialisation
WiFiClientSecure client;
UniversalTelegramBot bot(BOTtoken, client);

// Checks for new messages every 1 second.
int botRequestDelay = 1000;
unsigned long lastTimeBotRan;

//Led control initialization
const int ledPin = 25;
bool ledState = LOW;

//Screen's initialization
#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 64

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);


//temperature init
float temp = 20.0; 

//setup  NTP 
Timezone Paris; 

//initialisation of time to recheck temp
time_t t = Paris.now();

//Function to display text on the screen
void displayOnScreen(String text){
  display.clearDisplay();
  display.setTextSize(1);      // Normal 1:1 pixel scale
  display.setTextColor(WHITE); // Draw white text
  display.setCursor(0, 0); 
  display.print(text);
  display.display();
}

//Function to change temperature (recheck it if we had a sensor) every 10 seconds 
void generateTemp(time_t &timeLastChange, float &temp){
  if (Paris.now()>=(timeLastChange+10)){
    int change = -1 + (int)(((float)rand() * 3 )/ (RAND_MAX-1));
    temp = temp + change * 0.1;
    timeLastChange = Paris.now();
  }
}

//Permit to display our dashboard on the screen
void dashboard(){
  generateTemp(t,temp);
  displayOnScreen("Bonjour\n\nHeure : "+Paris.dateTime("H:i:s")+"\n\nDate : "+Paris.dateTime("j/m/Y")+"\n\nTemperature : "+temp);
}

// Handle what happens when you receive new messages
void handleNewMessages(int numNewMessages) {
  Serial.println("handleNewMessages");
  Serial.println(String(numNewMessages));

  for (int i=0; i<numNewMessages; i++) {
    // Chat id of the requester
    String chat_id = String(bot.messages[i].chat_id);
    if (chat_id != CHAT_ID){
      bot.sendMessage(chat_id, "Unauthorized user", "");
      continue;
    }
    
    
    // Print the received message
    String text = bot.messages[i].text;
    Serial.println(text);

    // Store the User that send the message 
    String from_name = bot.messages[i].from_name;

//------------------- Différentes commandes disponibles ! -------------------//
//Envoi la température capturé par le node sur le chat Telegram
    if (text == "/temp" ) {
      std::stringstream sstream; 
      sstream << temp; 
      String temp_str = (sstream.str()).c_str();
      bot.sendMessage(chat_id, "La temperature est de "+temp_str, "");
    }
//Affichage d'un message sur l'écran de la carte
    if (text.substring(0,8) == "/message" ) {
      displayOnScreen( from_name +" a dit :\n"+text.substring(9));
      sleep(3);
    }
//Envoi sur le chat Telegram les différentes commandes 
    if (text == "/help") {
      String help = "Bienvenue dans notre projet IoT "+ from_name+"!";
      help += "\nVoici les commandes disponibles :\n";
      help += "**/led** : commandes disponibles pour la led\n";
      help += "**/image** : liste des images à afficher sur l'ESP32\n";
      help += "**/temp** : je vous donne la température prise par l'ESP32\n";
      help += "**/time** : je vous donne l'heure\n";
      help += "**/date** : je vous donne la date";
      bot.sendMessage(chat_id, help, "Markdown");
    }
//Permet de d'afficher les commandes disponibles pour la LED et ajoute les boutons avec les commandes sur le chat Telegram
    if (text == "/led") {
      String keyboardJson = "[[\"/led_on\", \"/led_off\"],[\"/state\"]]";
      bot.sendMessageWithReplyKeyboard(chat_id, "Commandes disponibles :", "", keyboardJson, true);//Ajout boutons sur Telegram
    }
//Allume la LED et envoi sur le chat qu'elle est allumée
    if (text == "/led_on") {
      bot.sendMessage(chat_id, "LED state set to ON", "");
      ledState = HIGH;
      digitalWrite(ledPin, ledState);
      displayOnScreen("La led est allumee\npar "+ from_name +" !");
    }
//Eteint la LED et envoi sur le chat qu'elle est éteinte
    if (text == "/led_off") {
      bot.sendMessage(chat_id, "LED state set to OFF", "");
      ledState = LOW;
      digitalWrite(ledPin, ledState);
      displayOnScreen("La led est eteinte\npar "+ from_name +" !");
    }
//Envoi sur le chat si la LED est allumée ou éteinte
    if (text == "/state") {
      if (digitalRead(ledPin)){
        bot.sendMessage(chat_id, "LED is ON", "");
      }
      else{
        bot.sendMessage(chat_id, "LED is OFF", "");
      }
    }
//Envoi la date sur le chat Telegram
    if (text == "/date") {
      bot.sendMessage(chat_id, "On est le "+Paris.dateTime("j/m/Y"),"");
    }
//Envoi l'heure sur le chat Telegram
    if (text == "/time") {
      bot.sendMessage(chat_id, "L'heure est : "+Paris.dateTime("H:i:s"), "");
      displayOnScreen("Heure : "+Paris.dateTime("H:i:s"));
      sleep(4);
    }
//Permet de d'afficher les commandes disponibles pour afficher une image et ajoute les boutons avec les commandes sur le chat Telegram
    if (text == "/image") {
      String keyboardJson = "[[\"/mickey\", \"/enseeiht\"],[\"/bg\"]]";
      bot.sendMessageWithReplyKeyboard(chat_id, "Images disponibles:", "", keyboardJson, true);//Ajout boutons sur Telegram
    }
//Affiche l'image Mickey sur la carte
    if (text == "/mickey") {
      display.clearDisplay();
      display.drawBitmap(0, 0, mickey, 128, 64, 1);
      display.display();
      sleep(3);
    }
//Affiche l'image du logo de l'ENSEEIHT sur la carte
    if (text == "/enseeiht") {
      display.clearDisplay();
      display.drawBitmap(0, 0, enseeiht, 128, 64, 1);
      display.display();
      sleep(3);
    }
//Affiche l'image de Morgan CANAS sur la carte
    if (text == "/bg") {
      displayOnScreen("Attention aux yeux \n;)");
      sleep(2);
      display.clearDisplay();
      display.drawBitmap(0, 0, bg, 128, 64, 1);
      display.display();
      sleep(3);
    }
  }
}

void setup() {

  Serial.begin(115200);

  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { 
    Serial.println(F("SSD1306 allocation failed"));
    for(;;); // Don't proceed, loop forever
  }
  
  // Show n7 at start
  display.clearDisplay();
  display.drawBitmap(0, 0, enseeiht, 128, 64, 1);
  display.display();
  delay(2000); // Pause for 2 seconds

  // Clear the buffer
  display.clearDisplay();

  #ifdef ESP8266
    client.setInsecure();
  #endif

  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, ledState);
  
  // Connect to Wi-Fi
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi..");
  }

  //initialize ntp Client 
  waitForSync();
  Paris.setLocation("Europe/Paris");  

  bot.sendMessage(CHAT_ID, "C'est parti ! \n/help pour toutes les infos", "");
}

void loop() {
  if (millis() > lastTimeBotRan + botRequestDelay) {
    dashboard();
    int numNewMessages = bot.getUpdates(bot.last_message_received + 1);

    while(numNewMessages) {
      Serial.println("got response");
      handleNewMessages(numNewMessages);
      numNewMessages = bot.getUpdates(bot.last_message_received + 1);
      dashboard();
    }
    lastTimeBotRan = millis();
  }
}