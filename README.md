# ProjetIot

Morgan Canas, Lucas Lafage, Nicolas Rabez, Raphaël Sombstay et Brian Thomas

<h2>Présentation</h2> 

Dans le cadre de notre formation, nous avons mené un projet visant à manipuler des équipements utilisés en IoT. Ce projet utilise donc une carte TTGO ESP32-Paxcounter pouvant se connecter en Wi-Fi, LoRaWAN et Bluetooth.
![alt text](img/ecran_eteint.jpg "Image esp32")

Pour ce projet, nous devions nous inspirer d'un ou plusieurs des trois sujets qui nous étaient proposés. Nous avons choisi de travailler à partir de l'interfaçage de Telegram avec le node afin de pouvoir envoyer des commandes au node dans le but de récupérer des informations ou effectuer des actions sur celui-ci.

Notre projet n'étant pas motivé par une problématique particulière, nous avons décidé de réaliser un node multi-usages contrôlable à distance, capable de transmettre des informations et implémentant plusieurs fonctions.
Le node dispose d'un tableau de bord qui permet d'afficher directement sur l'écran des données comme l'heure, la date ou la température à la manière d'un bracelet connecté ou d'une montre connectée.

<h2>Fonctionnement avec Telegram</h2>


Pour utiliser le node avec Telegram nous avons utilisé la librairie **UniversalTelegramBot**. 

Cette librairie permet, sous réserve d'une connexion Internet fonctionnelle, de venir se connecter à un bot via l'API de **Telegram**. Pour la connexion à Internet nous avons choisi de travailler avec le **Wi-Fi** qui consomme plus d'énergie que LoRaWAN mais notre projet n'avait pas de contrainte énergétique particulière.

La librairie utilisée pour communiquer avec Telegram nous permet de récupérer tous les messages qui sont envoyés sur la conversation dans laquelle le bot est inclus. 

Les messages reçus sur la carte sont ensuite traités afin de reconnaître les commandes et lancer les actions qui en résultent. 

Nous avons donc créé plusieurs commandes présentées ci-dessous :
<ul> 
<li><b><I>/led</I></b>: qui permet d'afficher toutes les commandes en  rapport avec la LED (sélectionnable avec des boutons)
    <ul>
        <li><b><I>/state</I></b>: qui renvoie par message l'état courant de la LED </li>
        <li><b><I>/led_on</I></b>: qui permet d'allumer la LED et d'afficher sur l'écran qui a allumé la LED </li>
        <li><b><I>/led_off</I></b>: qui va éteindre la LED et afficher à l'écran le fait qu'elle est éteinte </li>
    </ul>
</li>
<li><b><I>/image</I></b>: : qui renvoie toutes les images qui peuvent être affichées sur le node (sélectionnable avec des boutons)
    <ul>
        <li><b><I>/enseeiht</I></b>: affiche le logo de l'ENSEEIHT</li>
        <li><b><I>/mickey</I></b>: affiche la tête de Mickey</li>
        <li><b><I>/bg</I></b>: affiche la tête de l'illustre Morgan Canas</li>
    </ul>
</li>
<li><b><I>/time</I></b>: qui va renvoyer le temps que l'on récupère via NTP il affiche aussi à l'écran</li>
<li><b><I>/date</I></b>: qui nous renvoie la date courante </li>
<li><b><I>/temp</I></b>: qui renvoie la température que l'on pourrait imaginer récupérer via un capteur mais pour l'instant il s'agit d'une température qui démarre à 20° et change aléatoirement de 0,1° toutes les 10 secondes</li>
<li><b><I>/help</I></b>: affiche dans la conversation Telegram les différentes actions et leur description</li>
</ul>

La mise en place et l'utilisation de la librairie Telegram est assez simple. Nous n'avons pas rencontré de problème particulier à sa mise en place. Du côté du Wi-Fi c'est la même chose, la librairie est assez simple et bien documentée. Pour tester notre projet avec votre propre installation, il faut remplacer les informations de connexion Wi-Fi (point d'accès et mot de passe) ainsi que le token du bot Telegram. Se référer à la documentation Telegram pour la mise en place d'un bot.

<h3>Gestion de la température</h3>

Pour cette partie, nous avons imaginé le cas où le node est connecté à un capteur de température. N'ayant pas ce type de capteur, nous avons simulé cette connexion. Pour cela on initialise la température à une valeur arbitraire (20°) qui pourrait être randomisée. Ensuite on utilise la fonction rand de C++ ainsi que la librairie ezTime (gestion du temps avec NTP) afin de pouvoir la modifier en fonction du temps. On va donc toutes les 10 secondes, récupérer un nombre entier entre -1 et 1. Celui-ci va nous permettre de savoir quelle transformation est appliquée à la température. On le multiplie ensuite par 0,1 et on l'additionne avec la température actuelle. Ceci nous permet de simuler le fait de récupérer la température auprès d'un capteur. 

Cette fonction pourrait être utilisée afin de placer par exemple des capteurs dans une pièce ou près des fenêtres afin de détecter les changements de température pour piloter le chauffage en utilisant une application backend. 

<h3>Récupération de l'heure et de la date via NTP</h3>

Afin de récupérer l'heure et la date de façon automatique via NTP, nous utilisons la librairie ezTime.<br>
Au départ nous avions prévu d'utiliser une librairie NTP classique mais les fonctions étaient beaucoup moins évoluées et plus compliquées à mettre en place surtout pour le changement d'heure. La librairie ezTime permet de récupérer le temps via NTP en fonction d'un fuseau horaire précis. Nous avons bien sûr choisi de récupérer le temps en fonction du fuseau horaire de Paris. On pourrait ici imaginer plusieurs solutions pour que cette récupération d'heure soit dynamique : 

<ul>
<li>Envoyer le fuseau horaire via Telegram ou une application backend sur le node</li>
<li>Connecter un capteur GPS, afin de récupérer les coordonnées du node et ainsi le positionner sur le bon fuseau horaire</li>
</ul>

La librairie est vraiment très simple d'utilisation, le seul point important est d'attendre après la connexion à Internet la première connexion NTP afin d'être sûr que l'on peut récupérer le temps via NTP.

<h3>Utilisation de l'écran</h3>

Le node est équipé d'un écran. L'affichage à l'écran est fait grâce à un ensemble de librairies *Adafruit*. Ces librairies permettent d'afficher du texte, des formes (rond, rectangle...) ou des images. 

On a pu alors afficher à l'écran diverses informations comme la température, l'heure, ou la date. Ces informations font notre tableau de bord. 

![alt text](img/dashboard_avant.jpg "Tableau de bord")

Les informations principales que reçoit le node passent par cet écran, que ce soit en temps réel avec le dashboard ou lorsqu'on lui envoie une commande. Par exemple, avec l'affichage de "X a allumé la LED" sur l'écran lorsque X envoie la commande <b><I>/led_on</I></b> sur Telegram. Les librairies <I>Adafruit</I> permettent aussi d'afficher des images, mais elle doivent être au format Bitmap. Pour afficher une image, il faut dimensionner l'image à la taille de l'écran (<I>128x64</I>) et la convertir au format Bitmap pour récupérer le code de la photo et l'ajouter dans le code du projet.



<h3>Utilisation de la LED et des différents PIN</h3>

La carte que nous avons possède une LED attachée au PIN 25. Il faut alors alimenter le PIN 25 si l'on veut allumer / éteindre la LED. Les autres PIN restent disponibles pour brancher d'autres équipements (capteurs par exemple).

![alt text](img/schema_carte.png "Carte TTGO")





<h2>Exemples:</h2>

Dans cette partie nous présentons certains résultats de notre projet :

<h3>Allumage de la carte</h3>

Lorsque le node est connecté en Wi-Fi à Internet, il indique, via l'API, au chat Telegram qu'il est disponible.

![alt text](img/defaut.jpg "Intro du bot")

On peut alors utiliser `/help` pour voir toutes les commandes disponibles !



<h3><I><b>/help</b></I></h3>

On envoie la commande `/help` dans le groupe où le bot est, on accède à toutes les commandes !

(il est suffisant de cliquer sur la commande en question au lieu de la réécrire) 

![alt text](img/help.jpg "/help")

### Gestion de la LED : 

`/help` permet d'avoir toutes les commandes disponibles pour la LED. Cette commande met des boutons à la place du clavier pour celui qui a envoyé le message, il suffit donc de cliquer sur le bouton que l'on veut !

![alt text](img/led.jpg "/led")

- A partir de Telegram la commande `/led_on` permet d'allumer la LED à distance. Une fois la commande interprétée par la carte, la LED verte ci-dessous s'allume.

![](img/dashboard_apres.jpg "Tableau de bord")

### Affichage des images

Comme présenté dans les parties précédente, il est possible d'afficher des images sur l'écran de l'ESP32. Lorsque l'on envoie `/image` , comme pour le `/led` , on a les différentes images disponibles à la place du clavier. On a juste à cliquer sur le bouton correspondant à l'image que l'on veut afficher pour l'afficher sur l'écran de la carte.

<h4>Exemple : affichage logo n7</h4>

Commande `/enseeiht`:

![](img/logo_n7.jpg "Affichage n7")



<h3>Récupérer des données du dashboard</h3>

D'autres commandes comme `/temp`, `/date` ou `/time` permettent de récupérer les informations du node que l'on affiche sur le dashboard. 

<h4>Température</h4>

![](img/temp.jpg "/temp")

<h4>Date</h4>

![](img/date.jpg "/date")

<h4>Heure</h4>

![](img/time.jpg "/time")
